;; Auto-generated. Do not edit!


(when (boundp 'my_msg_srv::Person_alone)
  (if (not (find-package "MY_MSG_SRV"))
    (make-package "MY_MSG_SRV"))
  (shadow 'Person_alone (find-package "MY_MSG_SRV")))
(unless (find-package "MY_MSG_SRV::PERSON_ALONE")
  (make-package "MY_MSG_SRV::PERSON_ALONE"))

(in-package "ROS")
;;//! \htmlinclude Person_alone.msg.html


(defclass my_msg_srv::Person_alone
  :super ros::object
  :slots (_name _age ))

(defmethod my_msg_srv::Person_alone
  (:init
   (&key
    ((:name __name) "")
    ((:age __age) 0)
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _age (round __age))
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:age
   (&optional __age)
   (if __age (setq _age __age)) _age)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; uint8 _age
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; uint8 _age
       (write-byte _age s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; uint8 _age
     (setq _age (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;;
   self)
  )

(setf (get my_msg_srv::Person_alone :md5sum-) "453db5a1253432b9f9c5ecabc5695077")
(setf (get my_msg_srv::Person_alone :datatype-) "my_msg_srv/Person_alone")
(setf (get my_msg_srv::Person_alone :definition-)
      "string name
uint8 age
")



(provide :my_msg_srv/Person_alone "453db5a1253432b9f9c5ecabc5695077")


