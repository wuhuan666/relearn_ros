
(cl:in-package :asdf)

(defsystem "my_msg_srv-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Person_alone" :depends-on ("_package_Person_alone"))
    (:file "_package_Person_alone" :depends-on ("_package"))
  ))