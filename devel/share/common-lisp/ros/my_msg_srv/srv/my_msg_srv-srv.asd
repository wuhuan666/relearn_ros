
(cl:in-package :asdf)

(defsystem "my_msg_srv-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "AddTwoInts_alone" :depends-on ("_package_AddTwoInts_alone"))
    (:file "_package_AddTwoInts_alone" :depends-on ("_package"))
  ))