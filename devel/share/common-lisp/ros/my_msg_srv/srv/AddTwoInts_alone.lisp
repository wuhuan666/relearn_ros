; Auto-generated. Do not edit!


(cl:in-package my_msg_srv-srv)


;//! \htmlinclude AddTwoInts_alone-request.msg.html

(cl:defclass <AddTwoInts_alone-request> (roslisp-msg-protocol:ros-message)
  ((A
    :reader A
    :initarg :A
    :type cl:integer
    :initform 0)
   (B
    :reader B
    :initarg :B
    :type cl:integer
    :initform 0))
)

(cl:defclass AddTwoInts_alone-request (<AddTwoInts_alone-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AddTwoInts_alone-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AddTwoInts_alone-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_msg_srv-srv:<AddTwoInts_alone-request> is deprecated: use my_msg_srv-srv:AddTwoInts_alone-request instead.")))

(cl:ensure-generic-function 'A-val :lambda-list '(m))
(cl:defmethod A-val ((m <AddTwoInts_alone-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_msg_srv-srv:A-val is deprecated.  Use my_msg_srv-srv:A instead.")
  (A m))

(cl:ensure-generic-function 'B-val :lambda-list '(m))
(cl:defmethod B-val ((m <AddTwoInts_alone-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_msg_srv-srv:B-val is deprecated.  Use my_msg_srv-srv:B instead.")
  (B m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AddTwoInts_alone-request>) ostream)
  "Serializes a message object of type '<AddTwoInts_alone-request>"
  (cl:let* ((signed (cl:slot-value msg 'A)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
  (cl:let* ((signed (cl:slot-value msg 'B)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AddTwoInts_alone-request>) istream)
  "Deserializes a message object of type '<AddTwoInts_alone-request>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'A) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'B) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AddTwoInts_alone-request>)))
  "Returns string type for a service object of type '<AddTwoInts_alone-request>"
  "my_msg_srv/AddTwoInts_aloneRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AddTwoInts_alone-request)))
  "Returns string type for a service object of type 'AddTwoInts_alone-request"
  "my_msg_srv/AddTwoInts_aloneRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AddTwoInts_alone-request>)))
  "Returns md5sum for a message object of type '<AddTwoInts_alone-request>"
  "713e5cf1444846805670f946f08bfc96")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AddTwoInts_alone-request)))
  "Returns md5sum for a message object of type 'AddTwoInts_alone-request"
  "713e5cf1444846805670f946f08bfc96")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AddTwoInts_alone-request>)))
  "Returns full string definition for message of type '<AddTwoInts_alone-request>"
  (cl:format cl:nil "int64 A~%int64 B~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AddTwoInts_alone-request)))
  "Returns full string definition for message of type 'AddTwoInts_alone-request"
  (cl:format cl:nil "int64 A~%int64 B~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AddTwoInts_alone-request>))
  (cl:+ 0
     8
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AddTwoInts_alone-request>))
  "Converts a ROS message object to a list"
  (cl:list 'AddTwoInts_alone-request
    (cl:cons ':A (A msg))
    (cl:cons ':B (B msg))
))
;//! \htmlinclude AddTwoInts_alone-response.msg.html

(cl:defclass <AddTwoInts_alone-response> (roslisp-msg-protocol:ros-message)
  ((Sum
    :reader Sum
    :initarg :Sum
    :type cl:integer
    :initform 0))
)

(cl:defclass AddTwoInts_alone-response (<AddTwoInts_alone-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <AddTwoInts_alone-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'AddTwoInts_alone-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name my_msg_srv-srv:<AddTwoInts_alone-response> is deprecated: use my_msg_srv-srv:AddTwoInts_alone-response instead.")))

(cl:ensure-generic-function 'Sum-val :lambda-list '(m))
(cl:defmethod Sum-val ((m <AddTwoInts_alone-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader my_msg_srv-srv:Sum-val is deprecated.  Use my_msg_srv-srv:Sum instead.")
  (Sum m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <AddTwoInts_alone-response>) ostream)
  "Serializes a message object of type '<AddTwoInts_alone-response>"
  (cl:let* ((signed (cl:slot-value msg 'Sum)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <AddTwoInts_alone-response>) istream)
  "Deserializes a message object of type '<AddTwoInts_alone-response>"
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'Sum) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<AddTwoInts_alone-response>)))
  "Returns string type for a service object of type '<AddTwoInts_alone-response>"
  "my_msg_srv/AddTwoInts_aloneResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AddTwoInts_alone-response)))
  "Returns string type for a service object of type 'AddTwoInts_alone-response"
  "my_msg_srv/AddTwoInts_aloneResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<AddTwoInts_alone-response>)))
  "Returns md5sum for a message object of type '<AddTwoInts_alone-response>"
  "713e5cf1444846805670f946f08bfc96")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'AddTwoInts_alone-response)))
  "Returns md5sum for a message object of type 'AddTwoInts_alone-response"
  "713e5cf1444846805670f946f08bfc96")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<AddTwoInts_alone-response>)))
  "Returns full string definition for message of type '<AddTwoInts_alone-response>"
  (cl:format cl:nil "int64 Sum~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'AddTwoInts_alone-response)))
  "Returns full string definition for message of type 'AddTwoInts_alone-response"
  (cl:format cl:nil "int64 Sum~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <AddTwoInts_alone-response>))
  (cl:+ 0
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <AddTwoInts_alone-response>))
  "Converts a ROS message object to a list"
  (cl:list 'AddTwoInts_alone-response
    (cl:cons ':Sum (Sum msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'AddTwoInts_alone)))
  'AddTwoInts_alone-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'AddTwoInts_alone)))
  'AddTwoInts_alone-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'AddTwoInts_alone)))
  "Returns string type for a service object of type '<AddTwoInts_alone>"
  "my_msg_srv/AddTwoInts_alone")