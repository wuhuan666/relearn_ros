#include<ros/ros.h>
#include"my_msg_srv/AddTwoInts_alone.h"
bool add(my_msg_srv::AddTwoInts_aloneRequest &req,
                    my_msg_srv::AddTwoInts_aloneResponse &res){
    res.Sum=req.A+req.B;
    ROS_INFO("request: x=%ld,y=%ld",(long int)req.A,(long int)req.B);
    ROS_INFO("singing back responce:[%ld]",(long int)res.Sum);
    return true;
}
//主函数
int main(int argc,char **argv){
    //创建节点
    ros::init(argc,argv,"add_two_ints_server");
    //创建节点句柄
    ros::NodeHandle n;
    //创建一个service，服务名称为add_two_ints，回调函数为add
    ros::ServiceServer server=n.advertiseService("add_two_ints",add);
    //阻塞函数
    ros::spin();
    return 0;
}