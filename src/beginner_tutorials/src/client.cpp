#include<ros/ros.h>
#include<cstdlib>
#include"my_msg_srv/AddTwoInts_alone.h"
int main(int argc,char **argv){
    //创建句柄
    ros::init(argc,argv,"add_two_ints_client");
    if(argc!=3){
        ROS_INFO("usage:add_two_int_client X Y");
        return 1;
    }
    //创建句柄
    ros::NodeHandle n;
    //创建客户端，消息类型为my_msr_srv::AddTwoInts_alone
    ros::ServiceClient client=n.serviceClient<my_msg_srv::AddTwoInts_alone>("add_two_ints");
    //创建自定义服务消息的类
    my_msg_srv::AddTwoInts_alone TwoInts;
    TwoInts.request.A=atoll(argv[1]);
    TwoInts.request.B=atoll(argv[2]);
    //服务调用
    if (client.call(TwoInts)){
        ROS_INFO("Sum:%ld",(long int)TwoInts.response.Sum);
    }
    else{
        ROS_INFO("Failed to call service add_two_ints");
        return 1;
    }
    return 0;
}