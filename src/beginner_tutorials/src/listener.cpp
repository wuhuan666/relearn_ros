#include<ros/ros.h>
#include"my_msg_srv/Person_alone.h"
//回调函数
void personCallback(const my_msg_srv::Person_alone::ConstPtr& person_alone){
    ROS_INFO("Hello [%s]",person_alone->name.c_str());
}
//主函数
int main(int argc,char **argv){
    //创建一个listener节点，注意节点唯一
    ros::init(argc,argv,"listener");
    //创建节点句柄
    ros::NodeHandle n;
    //创建一个subscriber
    ros::Subscriber person_sub=n.subscribe("Person",1000,personCallback);
    //阻塞函数
    ros::spin();
    return 0;
}