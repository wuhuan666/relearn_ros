//引用ros
#include<ros/ros.h>
//引用c++的用法
#include<sstream>
//引用自定义的消息类型
#include"my_msg_srv/Person_alone.h"
int main(int argc,char **argv){
    //创建talker节点，节点名称唯一
    ros::init(argc,argv,"talker");
    //创建节点句柄
    ros::NodeHandle n;
    //创建一个publisher
    ros::Publisher person_pub = n.advertise<my_msg_srv::Person_alone>("Person",1000);
    //设置发送频率，单位Hz
    ros::Rate loop_rate(10);
    //开始循环
    int count=0;
    while(ros::ok()){
        //创建自定义消息类
        my_msg_srv::Person_alone Person_alone;
        //字节流
        std::stringstream ss;
        //给ss赋值
        ss<<"Hua shou"<<count;
        //给发送消息赋值
        Person_alone.name=ss.str();
        Person_alone.age=count;
        //在终端显示消息
        ROS_INFO("%s",Person_alone.name.c_str());
        ROS_INFO("%d",Person_alone.age);
        //使用publisher把Person_alone发出去
        person_pub.publish(Person_alone);
        //阻塞函数
        ros::spinOnce();
        //根据刚才设置的频率休眠
        loop_rate.sleep();
        ++count;
    }
    return 0;
}