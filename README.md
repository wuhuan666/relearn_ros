# relearn_ros

#### 介绍
这个仓库是在https://zhuanlan.zhihu.com/p/384578650  
的基础上学习ros产生的仓库，用来温习ros的基础用法。  
感谢此链接的作者


#### 使用说明  
2.9.1节想要显示  
```
string name
uint8 age
```
需要将指令改为  
```
rosmsg show my_msg_srv/Person_alone 
```
2.9.2节若想显示
```
int64 A  
int64 B
---
int64 Sum
```
需要指令改为  
```
rossrv show my_msg_srv/AddTwoInts_alone
```
2.10.1节需要额外在beginner_tutorials包的package.xml添加
```
<build_depend>my_msg_srv</build_depend>
```
#### 3.23学了如何使用rviz/qt/tf/rosbag/launch文件